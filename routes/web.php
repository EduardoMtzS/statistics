<?php

Route::get('/', 'Valores@index');

Route::get('/chart', 'Statistics@chart');

Route::resource('statistics', 'Statistics');

/*
En resumen para crear todos
Route::resource('statistics', 'Statistics');
Para crear el controlador con todos los metodos
php artisan make:controller Statistics -r 

Route::get('/statistics', 'Statistics@index');

Route::get('/statistics/create', 'Statistics@create');

Route::get('/statistics/{project}', 'Statistics@show');

Route::post('/statistics', 'Statistics@store');

Route::patch('/statistics/{project}', 'Statistics@update');

Route::delete('/statistics/{project}', 'Statistics@destroy');
*/