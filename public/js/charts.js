$(document).ready(function () {
    var Names = new Array();
    var Wins = new Array();
    var Losses = new Array();
    var url = window.location.href;
    url = url.replace(/\/[^\/]*$/, '/chart');
    $.ajax({
        type: 'GET',
        url: url,
        success: function (response) {
            response.players.forEach(function (player) {
                Names.push(player.p_username);
                Wins.push(player.wins);
                Losses.push(player.losses);
            });
    
            var ctx = document.getElementById("canvas").getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: Names,
                    datasets: [{
                        label: 'Wins',
                        data: Wins,
                        backgroundColor: 'rgba(231, 037, 018, 0.8)',
                        borderColor: 'rgba(231, 037, 018, 1)',
                        borderWidth: 1
                    }, {
                        label: 'Losses',
                        data: Losses,
                        backgroundColor: 'rgba(030, 045, 110, 0.8)',
                        borderColor: 'rgba(030, 045, 110, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        },
        error: function (response) {
            console.log(response);
        }
    });
});
