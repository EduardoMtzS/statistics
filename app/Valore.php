<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Valore extends Model
{
    protected $fillable = ['p_id', 'p_username', 'kills', 'deaths', 'wins', 'losses', 'melees', 'played', 'bullets', 'headshots'];
}
