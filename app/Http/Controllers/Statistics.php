<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Valore;
use GuzzleHttp\Client;

class Statistics extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ids = [
            '8f2fc5e5-d0bc-45b7-93d7-6305ebce8b6c',
            '05723c0a-a9d6-46c2-8683-80f5f46a116f',
            '92c0ec01-745e-44ba-827e-446a0770a942',
            '734c8778-73a8-4487-9300-7416fcb303b2',
            '312f297c-47a5-4a3e-ae47-9ff7d9578927',
            'd7ea7089-922b-4be5-b9b2-d9f0f7978904',
            'f531c3fe-f887-4540-a794-7cb0d0b25a6a'
        ];
        $createOrUpdateUsers = Statistics::store($ids);
        $players = Valore::all();

        return view('statistics.index', compact('players'));
    }
    
    /**
     * Display a listing of the resource.
     */
    public function chart()
    {
        $players = Valore::all();

        return response()->json([
            'error' => false,
            'players' => $players,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($ids)
    {   
        $client = new Client(['base_uri' => 'https://r6tab.com/api/']);
        $updateUserDataClient = new Client();
        foreach($ids as $id){            
            $updateUserDataUrl = "https://r6tab.com/mainpage.php?page=$id&updatenow=true";
            $updateUserDataResponse = $updateUserDataClient->request('GET', $updateUserDataUrl);
            $params = "p_id=$id";
            $response = $client->request('GET', 'player.php', ['query' => $params]);
            $playerInfo = json_decode($response->getBody());
            $playerID = $playerInfo->p_id;
            $playerName = $playerInfo->p_name;
            $playerData = explode(',',str_replace(['"','[',']'],['','',''],$playerInfo->p_data));
            $kills = $playerData[1] + $playerData[6];
            $deaths = $playerData[2] + $playerData[7];
            $wins = $playerData[3] + $playerData[8];
            $losses = $playerData[4] + $playerData[9];
            $played = ($playerData[0]/3600) + ($playerData[5]/3600);
            $findPlayer = Valore::where('p_id',$id)->first();
            if(!$findPlayer){
                $newPlayer = Valore::create([
                    'p_id' => $playerID, 
                    'p_username' => $playerName, 
                    'kills' => $kills, 
                    'deaths' => $deaths, 
                    'wins' => $wins, 
                    'losses' => $losses, 
                    'melees' => $playerData[18], 
                    'played' => $played, 
                    'bullets' => $playerData[16], 
                    'headshots' => $playerData[17]
                ]);
            }else{
                $updatePlayer = $findPlayer->update([
                    'p_id' => $playerID, 
                    'p_username' => $playerName, 
                    'kills' => $kills, 
                    'deaths' => $deaths, 
                    'wins' => $wins, 
                    'losses' => $losses, 
                    'melees' => $playerData[18], 
                    'played' => $played, 
                    'bullets' => $playerData[16], 
                    'headshots' => $playerData[17]
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ids)
    {
        foreach($ids as $id){
            $deletedRows = Valore::where('p_id', $id)->delete();
        }
    }
}
