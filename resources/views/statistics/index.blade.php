@extends('layout')

@section('title','Estadisticas')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive-sm">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">User</th>
                        <th scope="col">Kills</th>
                        <th scope="col">Deaths</th>
                        <th scope="col">Wins</th>
                        <th scope="col">Losses</th>
                        <th scope="col">Melees</th>
                        <th scope="col">Played (hrs)</th>
                        <th scope="col">Bullets</th>
                        <th scope="col">Headshots</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($players as $player)
                    <tr>
                        <th scope="row">{{ $player->p_username }}</th>
                        <td>{{ number_format($player->kills) }}</td>
                        <td>{{ number_format($player->deaths) }}</td>
                        <td>{{ number_format($player->wins) }}</td>
                        <td>{{ number_format($player->losses) }}</td>
                        <td>{{ number_format($player->melees) }}</td>
                        <td>{{ number_format($player->played) }}</td>
                        <td>{{ number_format($player->bullets) }}</td>
                        <td>{{ number_format($player->headshots) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="card text-center">
            <div class="card-header">
                Grafica 1
            </div>
            <div class="car-body">
                <canvas id="canvas" height="280" width="600" style="position: relative;"></canvas>
            </div>
        </div>
    </div>
</div>
@endsection
